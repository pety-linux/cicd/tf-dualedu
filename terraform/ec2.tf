/*
# Master
resource "aws_instance" "master" {
  count                  = 1
  ami                    = var.image
  instance_type          = var.instance
  subnet_id              = aws_subnet.dmz.id
  vpc_security_group_ids = [aws_security_group.base.id]
  private_ip             = "10.0.1.69"
  user_data              = file("postinstall-m.sh")
# user_data = "${templatefile("${path.module}/bootstrap.tmpl",{BACKUP = ${var.BACKUP}})}"

  # OS disk
  root_block_device {
    volume_size           = "50"
    volume_type           = "gp2"
    encrypted             = false
    delete_on_termination = true
  }

  tags = {
    Name  = "master${count.index + 1}"
    Owner = var.owner
  }
}
*/

# Workers
resource "aws_instance" "worker" {
  count                  = 2
  ami                    = var.image
  instance_type          = var.instance
  subnet_id              = aws_subnet.dmz.id
  vpc_security_group_ids = [aws_security_group.base.id]
  user_data              = file("postinstall-m.sh")

  # OS disk
  root_block_device {
    volume_size           = "50"
    volume_type           = "gp2"
    encrypted             = false
    delete_on_termination = true
  }
  
  tags = {
    Name  = "worker${count.index + 1}"
    Owner = var.owner
  }
}

# Extra EBS Volumes for Workers
resource "aws_ebs_volume" "worker-vol-1" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-1"
  }
}

resource "aws_ebs_volume" "worker-vol-2" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-2"
  }
}

resource "aws_ebs_volume" "worker-vol-3" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-3"
  }
}

resource "aws_ebs_volume" "worker-vol-4" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-4"
  }
}

resource "aws_ebs_volume" "worker-vol-5" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-5"
  }
}

resource "aws_ebs_volume" "worker-vol-6" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-6"
  }
}

resource "aws_ebs_volume" "worker-vol-7" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-7"
  }
}

resource "aws_ebs_volume" "worker-vol-8" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-8"
  }
}

resource "aws_ebs_volume" "worker-vol-9" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-9"
  }
}

resource "aws_ebs_volume" "worker-vol-10" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-10"
  }
}

resource "aws_ebs_volume" "worker-vol-11" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-11"
  }
}

resource "aws_ebs_volume" "worker-vol-12" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-12"
  }
}

resource "aws_ebs_volume" "worker-vol-13" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-13"
  }
}

resource "aws_ebs_volume" "worker-vol-14" {
  count             = 2
  availability_zone = "us-east-1a"
  size              = 1

  tags = {
    Name = "worker${count.index + 1}-vol-14"
  }
}

# EBS Attachments for Workers
resource "aws_volume_attachment" "worker-vol-1-attach" {
  count       = 2
  device_name = "/dev/sdf"
  volume_id   = aws_ebs_volume.worker-vol-1.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}

resource "aws_volume_attachment" "worker-vol-2-attach" {
  count       = 2
  device_name = "/dev/sdg"
  volume_id   = aws_ebs_volume.worker-vol-2.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}

resource "aws_volume_attachment" "worker-vol-3-attach" {
  count       = 2
  device_name = "/dev/sdh"
  volume_id   = aws_ebs_volume.worker-vol-3.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}

resource "aws_volume_attachment" "worker-vol-4-attach" {
  count       = 2
  device_name = "/dev/sdi"
  volume_id   = aws_ebs_volume.worker-vol-4.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}

resource "aws_volume_attachment" "worker-vol-5-attach" {
  count       = 2
  device_name = "/dev/sdj"
  volume_id   = aws_ebs_volume.worker-vol-5.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}

resource "aws_volume_attachment" "worker-vol-6-attach" {
  count       = 2
  device_name = "/dev/sdk"
  volume_id   = aws_ebs_volume.worker-vol-6.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}

resource "aws_volume_attachment" "worker-vol-7-attach" {
  count       = 2
  device_name = "/dev/sdl"
  volume_id   = aws_ebs_volume.worker-vol-7.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}

resource "aws_volume_attachment" "worker-vol-8-attach" {
  count       = 2
  device_name = "/dev/sdm"
  volume_id   = aws_ebs_volume.worker-vol-8.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}

resource "aws_volume_attachment" "worker-vol-9-attach" {
  count       = 2
  device_name = "/dev/sdn"
  volume_id   = aws_ebs_volume.worker-vol-9.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}

resource "aws_volume_attachment" "worker-vol-10-attach" {
  count       = 2
  device_name = "/dev/sdo"
  volume_id   = aws_ebs_volume.worker-vol-10.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}

resource "aws_volume_attachment" "worker-vol-11-attach" {
  count       = 2
  device_name = "/dev/sdp"
  volume_id   = aws_ebs_volume.worker-vol-11.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}

resource "aws_volume_attachment" "worker-vol-12-attach" {
  count       = 2
  device_name = "/dev/sdr"
  volume_id   = aws_ebs_volume.worker-vol-12.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}

resource "aws_volume_attachment" "worker-vol-13-attach" {
  count       = 2
  device_name = "/dev/sds"
  volume_id   = aws_ebs_volume.worker-vol-13.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}

resource "aws_volume_attachment" "worker-vol-14-attach" {
  count       = 2
  device_name = "/dev/sdt"
  volume_id   = aws_ebs_volume.worker-vol-14.*.id[count.index]
  instance_id = element(aws_instance.worker.*.id, count.index)
}
