#!/bin/bash
# This scripts performs postinstall configuraton on deployed VMs.

exec > /tmp/debug.$$ 2>&1
set -x

# hostname
hostnamectl set-hostname dualedu --static

# Create User
useradd -s /bin/bash -c "tux" -m tux

echo 'tux:Dua1edu' | sudo chpasswd
#echo "Passw0rd" | passwd --stdin tux

# Set sudo
echo "tux ALL=(ALL)       NOPASSWD: ALL" >> /etc/sudoers

# Deploy SSH keys
mkdir /home/tux/.ssh
cat <<EOF | tee -a /home/tux/.ssh/authorized_keys
ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB+FcJU11IIvmuF+YGQTCjjshbAVHWGrbsrovA6bp9C5 Peter Barczi, pety.barczi@gmail.com
EOF
# Set proper permissions
chown -R tux /home/tux/.ssh
chmod 700 /home/tux/.ssh
chmod 600 /home/tux/.ssh/authorized_keys
##

# Adjust SSH
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config
systemctl restart sshd

## Add aliases
cat <<EOF | tee -a /etc/bashrc
# Aliases
alias s='sudo su -'
alias c=clear
EOF
source /etc/bashrc

cat <<EOF | tee -a /home/tux/.bashrc
# Aliases
alias s='sudo su -'
alias c=clear
EOF
chown tux:tux /home/tux/.bashrc 
source /home/tux/.bashrc
##

## Motd
unlink /etc/motd
cat <<EOF | tee -a /etc/motd
┌─────────────────────────────────────────────────────────────┐
│┌───────────────────────────────────────────────────────────┐│
││              Welcome to DualEdu Environment!              ││
││───────────────────────────────────────────────────────────││
││                                                           ││
││        This is your gate to Public Cloud world..          ││
││                                                           ││
│└───────────────────────────────────────────────────────────┘│
└─────────────────────────────────────────────────────────────┘
EOF
###
